import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPost } from '../actions';

class PostsShow extends Component { 

//    componentDidMount(){
//        const { id } = this.props.match.params;
//        console.log(id);
//        console.log(this.props.match.params);
//        this.props.fetchPost(id);
//   }

    render(){

        const { id } = this.props.match.params;
        const { post } = this.props;

        if (!post){
            return <div>Loading...</div>;
        }

        return(
            <div>
                <Link to="/" >Back to Index</Link>
                <h3>{post[id].title}</h3>
                <h6>Categories: {post[id].categories}</h6>
                <p>{post[id].content}</p>
            </div>
        );
    }
}

function mapStateToProps( state , ownProps){
    return { post : state.posts};
}

export default connect(mapStateToProps, { fetchPost }) (PostsShow);